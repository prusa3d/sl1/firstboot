Description:
  * Composed of 2 oneshot services
  * storetime.service: stores the time(hwclock -r) before it has a chance to be synchronized
    to the NTP
  * firstboot.service: oneshot, starts the firstboot.py script
  * firstboot.py: sends the preset data(RTC, IP address to the UART)
  * For the first boot, hwclock time is stored in /tmp/rtc.time
  * Both services are conditioned by ConditionFirstBoot, determined by presence of /etc/machine-id
  * Both services share a private /tmp folder


License:
  GPLv3, see LICENSE file

Questions:
  martin.kopecky@prusa3d.cz
