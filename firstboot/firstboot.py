#!/usr/bin/python3
"""
Sends RTC time and IP addresss over UART(to the test station).
Should be active only on the first boot.

"""
import os, sys
import logging
import logging.config
import traceback
import serial
import time


try: # deal with different variants of systemd.journal
    from systemd.journal import JournaldLogHandler
except:
    from systemd.journal import JournalHandler as JournaldLogHandler

logger = logging.getLogger()
logger.addHandler(JournaldLogHandler())
logger.setLevel(logging.DEBUG)


def get_ips(interface_name):
    """ Return all IP addresses for an interface """
    retcode = os.system(r""" ip addr show | grep {} |  grep -o "inet [0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" | grep -o "[0-9]*\.[0-9]*\.[0-9]*\.[0-9]*" >/tmp/ips.txt """.format(interface_name))
    if retcode != 0:
        return []
    ips = []
    with open("/tmp/ips.txt", "r") as f:
        ips = f.read().split('\n')
    ips = list(filter(lambda x: x != '', ips))
    return ips


class SerialSender:
    """ Handles sending the needed info over UART """
    def __init__(self):
        self.serial = None
        
    def open(self, device="/dev/ttyS2", baudrate=115200, timeout=60):
        """ Open the UART """
        self.serial = serial.Serial(device, baudrate=baudrate, parity="N", timeout=60)
        
    def send_ips(self, interface_name):
        """ Send IPs one per line as \nIP<10.24.10.12>IP\n """
        ips = get_ips(interface_name)
        if ips is not None:
            for ip in ips:
                print(f"Ip: {ip}")
                self.serial.write(bytes("\nIP<"+ip+">IP\n", 'ascii'))
            
    def send_date(self, filename="/tmp/rtc.time"):
        """ Send the RTC time as \nDATE<date>DATE\n
        It requires the storetime.service to be run first
        """
        rtc_time = None
        with open(filename, "r") as f:
            rtc_time = f.read().strip()
        record = "\nDATE<" + rtc_time + ">DATE\n"
        print(f"Date: {rtc_time}")
        self.serial.write(bytes(record, 'ascii'))
    
    def send_time_reliable(self):
        """ Report, if the RTC had a reliable time on startup(it should) """
        os.system("""dmesg | grep "low voltage detected, date/time is not reliable" > /tmp/reliable.txt""")
        result = False

        # If it fails, nothing will be written to UART. That is still a valid condition.
        with open("/tmp/reliable.txt", "r") as f:
            data = f.read()
            if data.strip() == '':
                result = True
        record = "\nTIME_RELIABLE<{}>TIME_RELIABLE\n".format(str(result))
        print(f"Time Reliable: {result}")
        self.serial.write(bytes(record, 'ascii'))
        
    def close(self):
        """ Signals the end of comunication, closes UART"""
        record = "\nEND.\n"
        print(record)
        self.serial.write(bytes(record, 'ascii'))
        self.serial.close()



if __name__ == "__main__" or True:
    try:
        logger.info("Running the FirstBoot service...")

        # Send informations to the tester, 2x just to be sure(noise on the line)
        print("Sending data over uart")
        ssender = SerialSender()
        ssender.open()
        ssender.send_ips("eth0")
        ssender.send_time_reliable()
        ssender.send_date()
        time.sleep(3)
        ssender.send_ips("eth0")
        ssender.send_time_reliable()
        ssender.send_date()
        ssender.close()
        exit(0)
        
    except SystemExit as e:
        raise e # propagate
        
    except:
        logger.error(traceback.format_exc())
        exit(1)
