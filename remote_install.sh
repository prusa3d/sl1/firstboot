#!/bin/sh

# Resolve target
if [ "$#" -ne 1  ]; then
    echo "Please provide target ip as the only argument"
    exit 1
fi
target=${1}
echo "Target is ${target}"


# Print commands being executed
set -o xtrace



# Solution: Start SSH multiplexing session (works fine)
# https://unix.stackexchange.com/questions/50508/reusing-ssh-session-for-repeated-rsync-commands
mkdir -p ~/.ssh/ssh-mux
ControlPath="~/.ssh/ssh-mux/%L-%r@%h:%p"
ssh -nNf -o ControlMaster=yes -o ControlPath="${ControlPath}" root@${target}



# Create temp root
tmp=$(mktemp --directory --tmpdir=/tmp/ firstboot.XXXX)
echo "Local temp is ${tmp}"

echo "Running setup"
python3 setup.py sdist --dist-dir=${tmp}

# Create remote temp
target_tmp=$(ssh -o "ControlPath=${ControlPath}" root@${target} "mktemp --directory --tmpdir=/tmp/ firstboot.XXXX")
echo "Remote temp is ${target_tmp}"

echo "Install on target...start"
scp -o "ControlPath=${ControlPath}" -r ${tmp}/* root@${target}:${target_tmp}
ssh -o "ControlPath=${ControlPath}" root@${target} "\
set -o xtrace; \
cd ${target_tmp}; \
systemctl stop firstboot.service;\
tar xvf firstboot-*.tar.gz; \
rm firstboot-*.tar.gz; \
cd firstboot-*; \
yes | pip3 uninstall firstboot; \
pip3 install . ; \
systemctl start sla-slicer-upload.service;\
"
echo "Install on target...done"

echo "Removing remote temp"
#ssh -o "ControlPath=${ControlPath}" root@${target} "rm -rf ${target_tmp}"

echo "Removing local temp"
rm -rf ${tmp}

#ssh -O exit  -o "ControlPath=${ControlPath}" roo@${target}
fuser -HUP -k "${HOME}/.ssh/ssh-mux/$(hostname)-root@${target}:22"

