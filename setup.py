from setuptools import setup, find_packages
from glob import glob

setup(
    name="firstboot",
    version="1.0",
    packages=find_packages(),
    scripts=['firstboot/firstboot.py'],
    package_data={'firstboot': []},
    data_files=[('/usr/bin', glob('firstboot/firstboot.py')),
        ("/usr/lib/systemd/system", glob('firstboot/firstboot.service')),
        ("/usr/lib/systemd/system", glob('firstboot/storetime.service'))],
    install_requires=["pydbus"]
)
